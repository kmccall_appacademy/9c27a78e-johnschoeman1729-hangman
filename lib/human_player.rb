class HumanPlayer

  def initialize; end

  def pick_secret_word
    print "Length of word: "
    gets.chomp.to_i
  end

  def check_guess(letter)
    puts "The computer guesses: #{letter}"
    print "Return indices: "
    gets.chomp.split(" ").map(&:to_i)
  end

  def register_secret_length(length); end

  def guess(_)
    print "guess a letter: "
    gets.chomp
  end

  def handle_response(letter, indices); end

  def candidate_words; end

end

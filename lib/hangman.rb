require_relative 'computer_player'
require_relative 'Human_player'

require 'byebug'

class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = []
  end

  def play
    setup
    while !game_over?
      take_turn
    end
    print_board
    puts "game over"
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    print_board
    guess = @guesser.guess(@board)
    indices = referee.check_guess(guess)
    update_board(guess, indices)
    @guesser.handle_response(guess, indices)
  end

  def update_board(letter, indices)
    indices.each do |i|
      @board[i] = letter
    end
  end

  def game_over?
    return true if @board.compact == @board
    false
  end

  def print_board
    res = @board.reduce("") { |str, ch| ch.nil? ? str << "_" : str << ch }
    puts "Secret Word: #{res}"
  end

end

if __FILE__ == $PROGRAM_NAME
  players = {
    guesser: ComputerPlayer.new,
    referee: ComputerPlayer.new
  }
  hm = Hangman.new(players)
  hm.play
end

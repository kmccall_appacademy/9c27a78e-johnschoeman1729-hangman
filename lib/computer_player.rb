class ComputerPlayer

  def initialize(dictionary = File.readlines("lib/dictionary.txt"))
    @dictionary = dictionary.map(&:chomp)
    @secret_word = ""
    @secret_length = 0
    @board = []
    @guesses = []
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(letter)
    indices = []
    @secret_word.each_char.with_index { |ch, i| indices << i if letter == ch }
    indices
  end

  def register_secret_length(length)
    @secret_length = length
    @board = Array.new(@secret_length)
  end

  def guess(board)
    @board = board
    board.each { |letter| @guesses.push(letter) }
    words = candidate_words
    most_frequent_letter_not_guessed(words)
  end

  def handle_response(letter, indices)
    @guesses.push(letter)
    return if indices.empty?
    indices.each { |i| @board[i] = letter }
  end

  def candidate_words
    words = @dictionary.select { |word| word.length == @secret_length }
    words = words.select { |word| word_could_match?(word.split(''), @board) }
    words.reject { |word| word_could_not_match?(word, @board) }
  end

  private

  def word_could_match?(word, board)
    word.each_index do |i|
      next if board[i].nil?
      return false if word[i] != board[i]
    end
    true
  end

  def word_could_not_match?(word, board)
    has_extra_guessed_letter?(word, board) || has_guessed_letter?(word)
  end

  def has_extra_guessed_letter?(word, board)
    temp = word.dup.split("")
    word.split("").each_index { |i| temp[i] = nil if word[i] == board[i] }
    temp.compact.any? { |letter| board.compact.include?(letter) }
  end

  def has_guessed_letter?(word)
    word.split("").any? { |ch| @guesses.include?(ch) && !@board.include?(ch) }
  end

  def most_frequent_letter_not_guessed(words)
    h = Hash.new(0)
    words.join('').each_char { |ch| h[ch] += 1 unless @guesses.include?(ch) }
    return h.max_by { |_, v| v }[0] unless h.empty?
    "I don't know this word."
  end

end
